﻿Imports VbFlexClientSettings.OpenOptions.dnaFusion.Flex.V1

Module Module1

    Sub Main()
        Dim apiKey = "a214ce66-ff77-4aee-8964-406f9817758e"
        Dim serviceUrl = "https://flex.ooinc.com/xmlrpc"

        Dim service = New FlexV1()
        service.Url = serviceUrl
        Dim settings = service.ClientSettings(apiKey)
        For Each item In settings
            Console.WriteLine("{0} = {1}", item.Name, item.Value)
        Next

        Console.ReadKey()
    End Sub

End Module
